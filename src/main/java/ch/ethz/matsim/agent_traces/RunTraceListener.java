package ch.ethz.matsim.agent_traces;

import java.util.Collection;
import java.util.LinkedList;

import org.locationtech.jts.geom.LineString;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.config.CommandLine;
import org.matsim.core.config.CommandLine.ConfigurationException;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.utils.geometry.geotools.MGC;
import org.matsim.core.utils.gis.PolylineFeatureFactory;
import org.matsim.core.utils.gis.ShapeFileWriter;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;

public class RunTraceListener {
	static public void main(String[] args)
			throws ConfigurationException, NoSuchAuthorityCodeException, FactoryException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("events-path", "output-path", "network-path") //
				.build();

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(cmd.getOptionStrict("network-path"));

		EventsManager eventsManager = EventsUtils.createEventsManager();

		TraceListener listener = new TraceListener(network);
		eventsManager.addHandler(listener);

		new MatsimEventsReader(eventsManager).readFile(cmd.getOptionStrict("events-path"));

		PolylineFeatureFactory featureFactory = new PolylineFeatureFactory.Builder() //
				.setCrs(MGC.getCRS("EPSG:2056")) //
				.setName("traces") //
				.create();

		Collection<SimpleFeature> features = new LinkedList<>();

		for (LineString geometry : listener.getTraces()) {
			SimpleFeature feature = featureFactory.createPolyline(geometry, new Object[] {}, null);
			features.add(feature);
		}

		ShapeFileWriter.writeGeometries(features, cmd.getOptionStrict("output-path"));
	}
}
