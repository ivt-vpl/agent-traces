package ch.ethz.matsim.agent_traces;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.locationtech.jts.geom.LineString;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkLeaveEvent;
import org.matsim.api.core.v01.events.PersonArrivalEvent;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.VehicleLeavesTrafficEvent;
import org.matsim.api.core.v01.events.handler.LinkLeaveEventHandler;
import org.matsim.api.core.v01.events.handler.PersonArrivalEventHandler;
import org.matsim.api.core.v01.events.handler.PersonDepartureEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleLeavesTrafficEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.vehicles.Vehicle;

public class TraceListener implements PersonDepartureEventHandler, PersonArrivalEventHandler,
		VehicleLeavesTrafficEventHandler, LinkLeaveEventHandler {
	private final Network network;
	private final Map<Id<Vehicle>, TraceBuilder> builders = new HashMap<>();
	private final Collection<LineString> traces = new LinkedList<>();

	public TraceListener(Network network) {
		this.network = network;
	}

	@Override
	public void handleEvent(PersonDepartureEvent event) {
		if (event.getLegMode().equals("car")) {
			Link link = network.getLinks().get(event.getLinkId());
			builders.put(Id.createVehicleId(event.getPersonId()), new TraceBuilder(link, event.getTime()));
		}
	}

	@Override
	public void handleEvent(LinkLeaveEvent event) {
		TraceBuilder trace = builders.get(event.getVehicleId());

		if (trace != null) {
			trace.addLink(network.getLinks().get(event.getLinkId()), event.getTime());
		}
	}

	@Override
	public void handleEvent(VehicleLeavesTrafficEvent event) {
		TraceBuilder trace = builders.get(event.getVehicleId());

		if (trace != null) {
			trace.addLink(network.getLinks().get(event.getLinkId()), event.getTime());
		}
	}

	@Override
	public void handleEvent(PersonArrivalEvent event) {
		TraceBuilder trace = builders.remove(Id.createVehicleId(event.getPersonId()));

		if (trace != null) {
			traces.add(trace.build());
		}
	}

	public Collection<LineString> getTraces() {
		return traces;
	}
}
