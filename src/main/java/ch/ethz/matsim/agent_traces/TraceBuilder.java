package ch.ethz.matsim.agent_traces;

import java.util.LinkedList;
import java.util.List;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.matsim.api.core.v01.network.Link;

public class TraceBuilder {
	private final static GeometryFactory factory = new GeometryFactory();

	private final List<Coordinate> coordinates = new LinkedList<>();

	public TraceBuilder(Link link, double time) {
		coordinates
				.add(new Coordinate(link.getFromNode().getCoord().getX(), link.getFromNode().getCoord().getY(), time));
	}

	public void addLink(Link link, double time) {
		coordinates.add(new Coordinate(link.getToNode().getCoord().getX(), link.getToNode().getCoord().getY(), time));
	}

	public LineString build() {
		return factory.createLineString(coordinates.toArray(new Coordinate[coordinates.size()]));
	}
}
